const {SALON_GAMES} = require("../../config.js");
const {EmbedBuilder, SlashCommandBuilder} = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName('duel')
        .setDescription('Lancer un duel contre un membre du discord.')
        .addUserOption(opt1 =>
            opt1.setName("user").setDescription("Name of the user you want to duel with.").setRequired(true)),
    permissions : [],
    channels : [SALON_GAMES],

    async runSlash(client, interaction) {

        const player = interaction.user;
        const target = interaction.options.getUser("user");

        if (player.equals(target)) {
            interaction.reply(
                `${interaction.member}, tu ne peux pas combattre avec toi même.`
            );
        } else if (target === client.user) {
            interaction.reply(`${interaction.member} René est imbattable...`);
        } else {
            interaction.reply("⚔️**Duel Fight !**");
            var lifeP1 = 100, lifeP2 = 100;
            var ms1 = null, ms2 = null;
            var stopSend = false;

            var r = 0;
            var att = [], def = [], round = [];

            const task = setInterval(() => {

                if (lifeP1 <= 0 || lifeP2 <= 0) {
                    stopSend = true;
                    clearInterval(task);
                    interaction.reply( {embeds: [sendEmbedResume()]});
                } else {
                    r++;
                    round.push(r);

                    var fight = parseInt(Math.random() * 2);
                    var hit = parseInt(Math.random() * 100);

                    var par = 100;
                    while (par > hit) {
                        par = parseInt(Math.random() * 100);
                    }

                    var hitR = hit - par;

                    if (fight === 0) {
                        lifeP2 = lifeP2 - hitR;
                        game(player, target, lifeP1, lifeP2, hit, hitR, par);
                    } else {
                        lifeP1 = lifeP1 - hitR;
                        game(target, player, lifeP2, lifeP1, hit, hitR, par);
                    }
                }
            }, 3000);
        }

        function game(playerAtt, playerDef, lifeAtt, lifeDef, hit, hitR, par) {
            if (!stopSend) {
                if (ms1 != null && ms2 != null) {
                    ms1.then((msg) => {
                        ms1 = msg.edit({embeds: [sendEmbedAtt(playerAtt, lifeAtt, hit)]});
                    });
                    ms2.then((msg) => {
                        ms2 = msg.edit({embeds: [sendEmbedDef(playerDef, lifeDef, par, hit, hitR)]});
                    });
                } else {
                    ms1 = interaction.reply({embeds: [sendEmbedAtt(playerAtt, lifeAtt, hit)]});
                    ms2 = interaction.reply(
                        {embeds: [sendEmbedDef(playerDef, lifeDef, par, hit, hitR)]}
                    );
                }
            }
        }

        function sendEmbedAtt(user, l1, hit) {
            att.push(user.username + ": " + `**${hit}**`);
            return new EmbedBuilder()
                .setColor("#e74c3c")
                .setTitle("⚔️ Attaque")
                .setThumbnail(`${user.displayAvatarURL()}`)
                .addFields(
                    {
                        name: "❤️ Vie:",
                        value: `${l1}/100`,
                        inline: false,
                    },
                    {
                        name: "🗡️ Infligés: ",
                        value: `${hit}`,
                        inline: false,
                    }
                );
        }

        function sendEmbedDef(user, l2, par, hit, hitR) {
            def.push(user.username + ": " + `**${hitR}**`);
            return new EmbedBuilder()
                .setColor("#3498db")
                .setTitle("🛡️ Défend")
                .setThumbnail(`${user.displayAvatarURL()}`)
                .addFields(
                    {
                        name: "❤️ Vie:",
                        value: `${l2}/100`,
                        inline: false,
                    },
                    {
                        name: "🗡️ Subit: ",
                        value: `${hit}`,
                        inline: true,
                    },
                    {
                        name: "🛡️ Bloqué: ",
                        value: `${par}`,
                        inline: true,
                    },
                    {
                        name: "💔 PDV retirés: ",
                        value: `${hitR}`,
                        inline: true,
                    }
                )
                .setFooter({ text: `Détails: (${hit} - ${par} -> ${hitR})`});
        }

        function sendEmbedResume() {
            var winner;
            if (lifeP1 <= 0) {
                winner = target.username;
            } else {
                winner = player.username;
            }
            return new EmbedBuilder()
                .setColor("#FFC312")
                .setTitle("Résumé du combat")
                .addFields(
                    {
                        name: "Round:",
                        value: `${round.join("\n")}`,
                        inline: true,
                    },
                    {
                        name: "Attaques:",
                        value: `${att.join("\n")}`,
                        inline: true,
                    },
                    {
                        name: "Défenses:",
                        value: `${def.join("\n")}`,
                        inline: true,
                    },
                    {
                        name: "🏆 Vainqueur:",
                        value: `**${winner}**`,
                        inline: false,
                    }
                )
        }
    }
};
