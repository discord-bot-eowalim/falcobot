const { SALON_GAMES } = require("../../config.js");
const {SlashCommandBuilder} = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName('lottery')
        .setDescription('Jouer à la lotterie.'),
    permissions : [],
    channels : [SALON_GAMES],

    async runSlash(client, interaction) {
        const tab = ["🔵", "🔴"];

        const one = parseInt(Math.random() * 2);
        const two = parseInt(Math.random() * 2);
        const three = parseInt(Math.random() * 2);

        interaction.reply(tab[one] + tab[two] + tab[three]);

        const tot = one + two + three;

        if (tot === 0 || tot === 3) {
            interaction.reply(`Félicitation ${message.author}, tu as gagné.`);
        }
    }
};

