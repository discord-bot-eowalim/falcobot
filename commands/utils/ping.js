const {SALON_BOT} = require("../../config.js");
const {SlashCommandBuilder, SelectMenuBuilder, ActionRowBuilder} = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName('ping')
        .setDescription('Ping command'),
    permissions : [],
    channels : [SALON_BOT],

    async runSlash(client, interaction) {
        interaction.reply("Pong avec slash");
    }
};

