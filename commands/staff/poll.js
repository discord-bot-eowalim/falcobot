const {EmbedBuilder, SlashCommandBuilder} = require("discord.js");
const {STAFF, SALON_BOT} = require("../../config.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName('poll')
        .setDescription('Créer un sondage interactif.')
        .addStringOption(opt =>
            opt.setName("question").setDescription("The question.").setRequired(true))
        .addStringOption(opt =>
            opt.setName("answers").setDescription("Answers separated by a comma.").setRequired(true)),
    permissions: [STAFF],
    channels: [SALON_BOT],

    async runSlash(client, interaction) {
        const question = interaction.options.getString("question");
        const answers = interaction.options.getString("answers").split(",");

        const rep_emoji = [];

        const emoji = [
            "0️⃣",
            "1️⃣",
            "2️⃣",
            "3️⃣",
            "4️⃣",
            "5️⃣",
            "6️⃣",
            "7️⃣",
            "8️⃣",
            "8️⃣",
            "🔟",
        ];

        if (answers.length !== 3) {
            for (let i = 1; i < answers.length; i++) {
                rep_emoji.push(`\n${emoji[i]} : ${answers[i]}`);
            }
        } else {
            rep_emoji.push(`\n✅ : ${answers[1]}`);
            rep_emoji.push(`\n❌ : ${answers[2]}`);
        }

        if (answers.length < 11) {
            const embed = new EmbedBuilder()
                .setColor("#6ab04c")
                .setTitle("Sondage")
                .setThumbnail(`${client.user.displayAvatarURL()}`)
                .addFields(
                    {
                        name: `Question`,
                        value: `${question}`,
                        inline: false,
                    },
                    {
                        name: "Réponses",
                        value: `${rep_emoji.join("")}`,
                        inline: false,
                    }
                )
                .setFooter({text: `Sondage crée par: ${interaction.user.username}`});

            const message = await interaction.reply({ embeds: [embed], fetchReply: true });
            for (let i = 1; i < answers.length; i++) {
                if (answers.length !== 3) {
                    message.react(emoji[i]);
                } else {
                    message.react("✅");
                    message.react("❌");
                }
            }
        } else {
            interaction.reply("Tu ne peux pas mettre plus de 10 réponses.");
        }
    }
}
