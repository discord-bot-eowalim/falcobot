const {EmbedBuilder, SlashCommandBuilder} = require("discord.js");
const {STAFF, LOGS} = require("../../config.js");
const {SALON_BOT} = require("../../config");
const fs = require("fs");
const path = "./files/antiRaid.json";

module.exports = {
    data: new SlashCommandBuilder()
        .setName('raidmode')
        .setDescription('Enable/Disable raidmode'),
    permissions: [STAFF],
    channels: [SALON_BOT],

    async runSlash(client, interaction) {
        var value;

        fs.readFile(path, (err, data) => {
            if (err) throw err;
            let raid = JSON.parse(data);
            changeMode(raid.raidMode);
        });

        function changeMode(actualMode) {
            value = !actualMode;
            let newMode = {raidMode: value};
            let data = JSON.stringify(newMode, null, 2);
            fs.writeFile(path, data, (err) => {
                if (err) throw err;
            });

            var mode;
            var color;
            if (value) {
                color = "#6ab04c";
                mode = "activé";
            } else {
                color = "#ff5252";
                mode = "désactivé";
            }

            const embed = new EmbedBuilder()
                .setColor(color)
                .setDescription(
                    `:information_source: **Le mode raid a été ${mode} par ${interaction.user.name}.**`
                );

            interaction.guild.channels.cache.forEach((channel) => {
                if (channel.name === LOGS) {
                    client.channels.cache.get(channel.id).send({embeds: [embed]});
                }
            });
        }
    }
};
