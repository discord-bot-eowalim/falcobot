const {EmbedBuilder, SlashCommandBuilder} = require("discord.js");
const {SALON_BOT, STAFF, BOT} = require("../../config.js");


module.exports = {
    data: new SlashCommandBuilder()
        .setName('user')
        .setDescription('Envoie les informations l\'utilisateur mentionné.')
        .addUserOption(opt1 =>
            opt1.setName("user").setDescription("Name of the user you want to duel with.").setRequired(true)),
    permissions: [],
    channels: [SALON_BOT],

    async runSlash(client, interaction) {


        const target = interaction.options.getUser("user");
        const targetMember = interaction.guild.members.cache.get(target.id);

        const roles = fillTabRole();

        const embed = new EmbedBuilder()
            .setColor("#6ab04c")
            .setTitle(`${target.tag}`)
            .setDescription(`<@${target.id}>`)
            .setThumbnail(`${target.displayAvatarURL()}`)
            .addFields(
                {
                    name: `Arrivée sur ${interaction.guild.name}`,
                    value: `${target.joinedAt.toDateString()}`,
                    inline: true,
                },
                {
                    name: "Compte crée",
                    value: `${target.createdAt.toDateString()}`,
                    inline: true,
                },
                {name: "Statut", value: `${checkStatut()}`, inline: true},
                {
                    name: `Roles[${roles.length}]`,
                    value: `${checkRole()}`,
                    inline: false,
                }
            )
            .setFooter({text: `ID: ${target.id}`});

        interaction.reply({embeds: [embed]});

        function fillTabRole() {
            const arrayRole = [];
            targetMember.roles.cache.forEach((role) => {
                if (role.name != "@everyone") {
                    arrayRole.push(`<@&${role.id}> `);
                }
            });
            return arrayRole;
        }

        function checkRole() {
            var finalRole = "N'a pas de rôle !";
            if (roles.length != 0) {
                finalRole = roles.join("");
            }
            return finalRole;
        }

        function checkStatut() {
            var finalStatut = "Membre";
            targetMember.roles.cache.forEach((role) => {
                if (role.id === STAFF) {
                    finalStatut = "Modérateur";
                } else if (role.id === BOT) {
                    finalStatut = "BOT";
                }
            });
            return finalStatut;
        }
    }
};



