const {EmbedBuilder, SlashCommandBuilder} = require("discord.js");
const {SALON_BOT} = require("../../config.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName('help')
        .setDescription('Envoie la liste des commandes.'),
    permissions : [],
    channels : [SALON_BOT],

    async runSlash(client, interaction) {
        const embed = new EmbedBuilder()
            .setColor("#F97F51")
            .setTitle(`:pushpin: Aide ${client.user.username}`)
            .setDescription(`:round_pushpin: Préfix: **/**`)
            .setThumbnail(`${client.user.displayAvatarURL()}`)
            .setFooter({ text: `Bot by Eowalim v3.0 | En cas de bugs contactez Eowalim`});

        client.commands.map(cmd => {
            if(cmd.data){
                let commandName = "**/" + cmd.data.name + "**";
                let description = cmd.data.description;
                embed.addFields({ name: commandName, value: description, inline: false });
            }
        });

        interaction.reply({embeds : [embed]});
    }
};
