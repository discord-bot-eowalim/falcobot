const axios = require('axios');
const {EmbedBuilder, SlashCommandBuilder} = require("discord.js");
const {SALON_BOT, WEATHER_API_URL, WEATHER_TOKEN} = require("../../config.js");


module.exports = {
    data: new SlashCommandBuilder()
        .setName('weather')
        .setDescription('Donne les informations sur la météo du jour.')
        .addStringOption(opt =>
            opt.setName("cp").setDescription("Code postal.").setRequired(true)),
    permissions : [],
    channels : [SALON_BOT],

    async runSlash(client, interaction) {
        const code = interaction.options.getString("cp");
        const cp = parseInt(code)
        let name;
        axios.get(WEATHER_API_URL + '/api/location/cities', {params : {token : WEATHER_TOKEN , search: cp}})
            .then(
                r => {
                    const city = r.data.cities[0];
                    if(city === undefined){
                        interaction.reply(`${interaction.user} le code postal est incorrect.`);
                    } else {
                        const insee_val = parseInt(city.insee)
                        name = city.name;

                        axios.get(WEATHER_API_URL + '/api/forecast/daily ', {params : {token : WEATHER_TOKEN , insee: insee_val}}).then((r)=>{
                            const weather = r.data.forecast[0];
                            const date = new Date()
                            const embed = new EmbedBuilder()
                                .setColor("#3498db")
                                .setTitle(`${name}`)
                                .setDescription(`Prévision du ${date.toLocaleDateString("fr")}`)
                                .setThumbnail(`https://cdn.discordapp.com/attachments/832330476336840745/959129178979041350/m2H7K9K9d3i8b1b1-removebg-preview.png`)
                                .addFields(
                                    {
                                        name: "🌡️ Température",
                                        value: `Min: ${weather.tmin}°C \nMax : ${weather.tmax}°C \nMoy : ${(weather.tmin + weather.tmax)/2}°C`,
                                        inline: true,
                                    },
                                    {
                                        name: "💨 Vent moyen",
                                        value: `${weather.wind10m}km/h`,
                                        inline: true,
                                    },
                                    {
                                        name: "💧 Pluie",
                                        value: `\nQuantité max : ${weather.rr1}mm`,
                                        inline: true,
                                    },
                                    {
                                        name: "Probabilité",
                                        value: `Pluie : ${weather.probarain}% \n Gel : ${weather.probafrost}%\n Brouillard : ${weather.probafog}%\n Vent (>70 km/h) : ${weather.probawind70}%\n Vent (>100 km/h) : ${weather.probawind100}%`,
                                        inline: false,
                                    },

                                )
                                .setFooter({ text: `La météo par René`});
                            interaction.reply({embeds: [embed]});
                        })
                    }
                }
            )
    }
};