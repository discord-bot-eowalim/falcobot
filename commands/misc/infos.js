const {EmbedBuilder, SlashCommandBuilder} = require("discord.js");
const {SALON_BOT, STAFF} = require("../../config.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName('infos')
        .setDescription('Envoie les informations du serveur.'),
    permissions: [],
    channels: [SALON_BOT],

    async runSlash(client, interaction) {

        let getOwners = async () => {
            return await interaction.guild.fetchOwner().catch(err => err)
        }
        getOwners().then(owner => {
            if (owner !== undefined) {
                const embed = new EmbedBuilder()
                    .setColor("#f0932b")
                    .setTitle(`${interaction.guild.name}`)
                    .setThumbnail(`${interaction.guild.iconURL()}`)
                    .addFields(
                        {
                            name: "Crée le",
                            value: `${interaction.guild.createdAt.toDateString()}`,
                            inline: true,
                        },
                        {
                            name: "Fondateur",
                            value: `${owner.user.username}`,
                            inline: true,
                        },
                        {
                            name: "Membres",
                            value: `${interaction.guild.memberCount}`,
                            inline: true,
                        },
                        {
                            name: "Staff",
                            value: `${getStaff().join("")}`,
                            inline: false,
                        }
                    )
                    .setFooter({text: `Auteur: ${interaction.user.username}`});

                interaction.reply({embeds: [embed]});

                function getStaff() {
                    var staff = [];
                    interaction.guild.members.cache.forEach((mbr) => {
                        if (mbr.roles.cache.some((role) => role.id === STAFF)) {
                            staff.push(`- ${mbr.user.username}\n`);
                        }
                    });
                    return staff;
                }
            }
        })
    }
};

