const {SALON_BOT} = require("../../config.js");
const {SlashCommandBuilder} = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName('random')
        .setDescription('Tire au hasard un nombre dans un intervalle donné.')
        .addNumberOption(opt =>
            opt.setName("max").setDescription("Maximum bound of the interval.").setRequired(true))
        .addNumberOption(opt =>
            opt.setName("min").setDescription("Minimum bound of the interval.").setRequired(false)),
    permissions: [],
    channels: [SALON_BOT],

    async runSlash(client, interaction) {
        let min = 0;
        let max = 0;
        let inter = "["

        let a = interaction.options.getNumber("min");
        let b = interaction.options.getNumber("max");

        if(a === null){
            inter += "0, " + b + "]"
        } else {
            min = a > b ? b : a;
            max = a < b ? b : a;
            inter += min + ", " + max + "]"
        }
        interaction.reply(`${interaction.user} : **${Math.floor(Math.random() * (max - min + 1)) + min}** dans ${inter}`)
    }
};
