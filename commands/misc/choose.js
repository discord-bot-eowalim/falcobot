const { SALON_BOT } = require("../../config.js");
const {SlashCommandBuilder} = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName('choose')
        .setDescription('Tire au hasard un des choix donnés.')
        .addStringOption(opt =>
            opt.setName("choices").setDescription("Choices separated by a comma .").setRequired(true)),
    permissions : [],
    channels : [SALON_BOT],

    async runSlash(client, interaction) {
        const choices =  interaction.options.getString("choices").split(",");
        interaction.reply(`${interaction.user} j'ai choisi : ${choices[Math.floor(Math.random() * choices.length)]}`)
    }
};

