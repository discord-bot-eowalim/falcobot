const {SERVER} = require("../../config.js");
const path = "./files/antiRaid.json";
const fs = require("fs");

module.exports = {
    name: 'guildMemberAdd',
    once: false,
    execute(client, member) {
        fs.readFile(path, (err, data) => {
            if (err) throw err;
            let raid = JSON.parse(data);
            check(raid.raidMode);
        });

        function check(mode) {
            let server = null;
            client.guilds.cache.forEach((guilds) => {
                if (guilds.id === SERVER) {
                    server = guilds;
                }
            });

            if (mode) {
                server.member(member).kick().then(r => console.log("Member has been kicked => RaidMod on"));
            }
        }
    }
};

