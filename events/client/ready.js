const {SERVER} = require("../../config.js");

module.exports = {
    name: 'ready',
    once: true,
    async execute(client) {
        console.log(`>> ${client.user.tag} is ready! <<`);
        const devGuild = await client.guilds.cache.get(SERVER);
        client.commands.map(cmd => {
            if(cmd.data){
                devGuild.commands.create(cmd.data);
            }
        });
    }
};