
module.exports = {
    name : 'interactionCreate',
    once : false,
    async execute(client, interaction) {
        if (interaction.isCommand()){
            const cmd = client.commands.get(interaction.commandName);
            if (!cmd) interaction.reply(`${interaction.user}, cette commande n\'existe pas.`);

            let has_permission = false;

            cmd.permissions.forEach(p => {
                const member_perm = interaction.member.roles.cache.map(r => r.id);
                if (has_permission === false && member_perm.includes(p)) {
                    has_permission = true;
                }
            })

            if (!cmd.permissions && !has_permission) return interaction.reply(`${interaction.user}, tu n'as pas la permission d\'exécuter cette commande.`);

            if (!cmd.channels.includes(interaction.channelId)){
               return interaction.reply(`${interaction.user}, cette commande n\'est pas autorisée dans ce salon.`);
            }

            cmd.runSlash(client, interaction);
        }



        else if (interaction.isSelectMenu()) {

        }


    }
};