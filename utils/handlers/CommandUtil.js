const {promisify} = require('util');
const {glob} = require('glob');
const pGlob = promisify(glob);

module.exports = async client => {
    (await pGlob(`${process.cwd()}/commands/*/*.js`)).map(async cmdFile => {
        const cmd = require(cmdFile);

        if (!cmd.data)
            return console.log(`Unloaded command: ${cmd.name} => The data is missing.`)

        client.commands.set(cmd.data.name, cmd);
        console.log(`(CMD) - ${cmd.data.name} loaded`)
    })
};
