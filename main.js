const {Client, Collection, IntentsBitField} = require("discord.js");
const {TOKEN} = require("./config.js");

const intents = new IntentsBitField(3276799);
const client = new Client({intents});

client.commands = new Collection();

['CommandUtil', "EventUtil"].forEach(handler => { require(`./utils/handlers/${handler}`)(client)})

client.login(TOKEN).then(r => console.log(`>> ${client.user.tag} has been logged. <<`))